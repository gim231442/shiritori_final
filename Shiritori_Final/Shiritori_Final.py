# -*- coding: utf-8 -*-
import json
import os
import re
import time
import urllib.request
from urllib import parse
from bs4 import BeautifulSoup
import random
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

######## block
from flask import request
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
###############


SLACK_TOKEN = "xoxb-682997044292-683285269889-5qkGic4evScVu7tEVgco8wKg"
SLACK_SIGNING_SECRET = "0ac44598c4c4f620ce9bd221a2f4e238"

re_error = re.compile('[a-zA-Z | \d]')

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

prev_client_msg_id = {}

state = {}
lose_rank = {}
win_rank = {}
s_message = ""
searchPage = 0
score = 0
name = ""

false_State = 0
def get_score(pair):
    return pair[1]
    
# 컴퓨터가 말할 단어 검색해서 크롤링 
def _crawl_word_list(text):
   # if not "music" in text:
   #     return "`@<봇이름> music` 과 같이 멘션해주세요."

    searchText = text[-2] + text[-1]
    global searchPage
    global state
    word_list = []
    for i in range(1, searchPage + 1):
        url1 = "krdict.korean.go.kr/dicSearch/search?wordMatchFlag=N&currentPage=" + str(i) + "&mainSearchWord="
        url2 = searchText
        url3 = "&sort=W&searchType=W&proverbType=&exaType=&ParaWordNo=&divSearch=search&deleteWord_no=&nationCode=&returnUrl=&downloadInfo=&downloadInfoText=&downloadGubun=&downloadType=&downloadItemList=&downloadMultilanList=&priMoveUrl=&blockCount=10"
        FullURL = url1 + url2 + url3
        url = "https://" + parse.quote(FullURL)

        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
   
        for words in soup.find_all("li", class_="printArea"):
            if words.find("em") is not None:
                if "명사" in words.find("em").get_text():
                    line = words.find("strong").get_text()
                    if len(line) > 1 and state.get(line) is None:
                        state[line] = True
                        word_list.append(line + "!")
                        
    word_list = list(set(word_list))
    if len(word_list) == 0: return ""
    else: return random.choice(word_list)

# 유저가 입력한 단어가 명사인지 체크하는 함수
def _crawl_user_word_check(text):
   # if not "music" in text:
   #     return "`@<봇이름> music` 과 같이 멘션해주세요."

   searchText = text
   global false_State

   # 단어가 존재 하는지 체크
   url1 = "krdict.korean.go.kr/dicSearch/search?wordMatchFlag=N&currentPage=1&mainSearchWord="
   url2 = searchText
   url3 = "&sort=W&searchType=W&proverbType=&exaType=&ParaWordNo=&divSearch=search&deleteWord_no=&nationCode=&returnUrl=&downloadInfo=&downloadInfoText=&downloadGubun=&downloadType=&downloadItemList=&downloadMultilanList=&priMoveUrl=&blockCount=10&myViewWord=40025&myViewWord=31451"
   FullURL = url1 + url2 + url3
   url = "https://" + parse.quote(FullURL)

   source_code = urllib.request.urlopen(url).read()
   soup = BeautifulSoup(source_code, "html.parser")

   frame = soup.find("div", class_="search_tit")
   words = frame.find("span", class_="num_red")
   if words.get_text().strip() == '0':
       false_State = 1
       print("! 없는 단어 입니다.")
       return False

   # 존재한다면 명사인지 체크
   for words in soup.find_all("li", class_="printArea"):
       if words.find("em") is not None:
           if "명사" in words.find("em").get_text():
               if words.find("strong").get_text().strip() == searchText:
                   print("! 명사가 맞습니다.")
                   return True

   print("! 명사가 아닙니다.")
   false_State = 2

   return False




# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    
    client_msg_id = event_data["event"]["client_msg_id"]
    if client_msg_id not in prev_client_msg_id:
        prev_client_msg_id[client_msg_id] = 1
    else: return

    global s_message
    global searchPage
    global score
    global name
    global lose_rank
    global win_rank
    u_message = text.split()[1]
    s_state = False
    if u_message.isdigit() and int(u_message) == 10:
        s_message = "초기화 완료"
        searchPage = 0
        name = ""
        score = 0
        global state
        state.clear()
        slack_web_client.chat_postMessage(
            channel=channel,
            text=s_message
        )
        s_message = ""
        return

    
    if u_message.isdigit() and int(u_message) == 11:
        print("ㅡㅡㅡㅡLose Rankingㅡㅡㅡㅡ")
        rank_list = [ (name, score) for name, score in lose_rank.items() ]
        sorted_list = sorted(rank_list, key = get_score, reverse = True)
        result = []
        for i, line in enumerate(sorted_list):
            result.append( str(i+1) + "위 : " + line[0] + " Score : " + str(line[1]) )
        print('\n'.join(result))
        return
    if u_message.isdigit() and int(u_message) == 12:
        print("ㅡㅡㅡㅡWin Rankingㅡㅡㅡㅡ")
        rank_list = [ (name, score) for name, score in win_rank.items() ]
        sorted_list = sorted(rank_list, key = get_score, reverse = True)
        result = []
        for i, line in enumerate(sorted_list):
            result.append( str(i+1) + "위 : " + line[0] + " Score : " + str(line[1]) )
        print('\n'.join(result))
        return

    #if u_message.isdigit() and int(u_message) == 11:
        #파일에 경로가 있는지 확인
        #있으면 읽어들어와서
        #순서대로 출력하기
        

    if searchPage == 0:
        if(u_message.isdigit() and len(text.split()) == 3 ):
            num = int(u_message)
            if num > 1 and num < 6:
                searchPage = num
                name = text.split()[2]
                s_message = "난이도 설정 완료\n\n반갑습니다 "+ name + "님, 먼저 시작하시죠 !"
            else : s_message = "안녕하세요. 끝말잇기 챗봇 cookie입니다. \n\ncookie는 한국어 기초사전에 등재되있는 단어를 기준으로 플레이 할 수 있습니다. \n\n게임을 시작하기 위해서 난이도를 설정해주세요 (2~5) \n ex) 2 김민철"
        else: s_message = "안녕하세요. 끝말잇기 챗봇 cookie입니다. \n\ncookie는 한국어 기초사전에 등재되있는 단어를 기준으로 플레이 할 수 있습니다. \n\n게임을 시작하기 위해서 난이도를 설정해주세요 (2~5) \n ex) 2 김민철"
        
        slack_web_client.chat_postMessage(
            channel=channel,
            text=s_message
        )
        s_message = ""
    elif searchPage > 1 and searchPage < 6:
        if s_message != "":
            if s_message[0] == "ㅡ":
                slack_web_client.chat_postMessage(
                    channel=channel,
                    text=s_message
                )
                return


        if re_error.search(u_message):
            s_message = "ㅡㅡㅡㅡYou Loseㅡㅡㅡㅡ\n\n단어가 아니에요 !\n" + name + "\nScore : " + str(score)
            print("A")
        elif s_message != "" and s_message[0] != "ㅡ" and s_message[-2] != u_message[0]:
            s_message = "ㅡㅡㅡㅡYou Loseㅡㅡㅡㅡ\n\n끝말이 틀렸어요 !\n" + name + "\nScore : " + str(score)
            print("B")
        elif state.get(u_message) is not None:
            s_message = "ㅡㅡㅡㅡYou Loseㅡㅡㅡㅡ\n\n이미 말했던 단어에요 !\n" + name + "\nScore : " + str(score)
            print("C")
        else :
            if _crawl_user_word_check(u_message):
                print("D")
                score += 1
                s_state = True
                state[u_message] = True
            else :
                if false_State == 2:
                    s_message = "ㅡㅡㅡㅡYou Loseㅡㅡㅡㅡ\n\n명사가 아니에요 !!\n" + name + "\nScore : " + str(score)
                else:
                    s_message = "ㅡㅡㅡㅡYou Loseㅡㅡㅡㅡ\n\n없는 단어에요 !!\n" + name + "\nScore : " + str(score)
                print("E")

        if s_message != "" and s_message[0]:
            if lose_rank.get(name) is None:
                lose_rank[name] = score
            else:
                if lose_rank[name] < score: lose_rank[name] = score
                    
        #print(state)
        #time.sleep(1)
        print(u_message)
        print(s_message)
        if s_state:
            print("F")
            s_message = _crawl_word_list(u_message[-1] + "*")
            if s_message == "":
                print("G")
                s_message = "ㅡㅡㅡㅡYou Winㅡㅡㅡㅡ\n'" + u_message[-1] + "' 으로 시작하는 단어는 제 생각에 없는거 같아요.. 당신이 이겼어요!\n"+ name + "\nScore : " + str(score)
                if win_rank.get(name) is None:
                    win_rank[name] = score
                else:
                    if win_rank[name] < score: win_rank[name] = score     

        if s_message != "" and s_message[0] == "ㅡ":
            slack_web_client.chat_postMessage(
            channel=channel,
            blocks=extract_json(init_info_button())
            ) 
        else :
            slack_web_client.chat_postMessage(
                channel=channel,
                text=s_message
            )

#### 초기화 버튼 ####
def init_info_button():
    text_block = SectionBlock(
        text=s_message
    )

    button_actions = ActionsBlock(
        block_id="init_info",
        elements=[
            ButtonElement(
                text="한번 더 하기!", style="primary",
                action_id="init_1", value=str(1)
            ),
            ButtonElement(
                text="랭킹 보기!", style="danger",
                action_id="init_2", value=str(2)
            ),
        ]
    )
    
    return [text_block, button_actions]

def view_ranking():
    global s_message
    global score
    global name
    global lose_rank
    global win_rank

    print("ㅡㅡㅡㅡLose Rankingㅡㅡㅡㅡ")
    rank_list = [ (name, score) for name, score in lose_rank.items() ]
    sorted_list = sorted(rank_list, key = get_score, reverse = True)
    result1 = []
    for i, line in enumerate(sorted_list):
        result1.append( str(i+1) + "위 : " + line[0] + " Score : " + str(line[1]) )
    print('\n'.join(result1))

    print("ㅡㅡㅡㅡWin Rankingㅡㅡㅡㅡ")
    rank_list = [ (name, score) for name, score in win_rank.items() ]
    sorted_list = sorted(rank_list, key = get_score, reverse = True)
    result2 = []
    for i, line in enumerate(sorted_list):
        result2.append( str(i+1) + "위 : " + line[0] + " Score : " + str(line[1]) )
    print('\n'.join(result2))
    r1 = '\n'.join(result1) + "\n"
    r2 = '\n'.join(result2) + "\n"
    text_block = SectionBlock(
        text="ㅡㅡㅡㅡLose Rankingㅡㅡㅡㅡ\n" + r1 + "ㅡㅡㅡㅡWin Rankingㅡㅡㅡㅡ\n" + r2
    )

    return [text_block]
 
def info_init():
    global s_message
    global searchPage
    global score
    global name
    global state
    s_message = ""
    searchPage = 0
    name = ""
    score = 0
    state.clear()

    text_block = SectionBlock(
        text="내 정보 초기화 완료!\n아무 글자나 입력하면 새로 시작합니다!"
    )

    return [text_block]


# 버튼 이벤트
@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    print("button_click")
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    print(click_event.action_id)
    if click_event.value == '1':
        # 초기화를 진행합니다.
        message_blocks = info_init()
    elif click_event.value == '2':
        message_blocks = view_ranking()

    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200

############

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    if os.path.isfile("win_rank.txt"):
        pass
    else:
        di = {}
        lose_json = json.dumps(di)
        with open("win_rank.txt", "w") as file:
            file.write(json.dumps(di))

    if os.path.isfile("lose_rank.txt"):
        pass
    else:
        di = {}
        lose_json = json.dumps(di)
        with open("lose_rank.txt", "w") as file:
            file.write(json.dumps(di))


    with open("win_rank.txt", "r") as file:
        win_rank = json.loads(file.read())
    with open("lose_rank.txt", "r") as file:
        lose_rank = json.loads(file.read())
    app.run('0.0.0.0', port=5000)
    lose_json = json.dumps(lose_rank)
    with open("win_rank.txt", "w") as file:
        file.write(json.dumps(win_rank))
    with open("lose_rank.txt", "w") as file:
        file.write(json.dumps(lose_rank))        
    